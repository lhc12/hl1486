This repository is created for submission of the CIF2014 simulation assignment using JavaCL and Maven, by Haicheng Lu

Run the program:

1. Open Git Bash, input command: "git clone https://lhc12@bitbucket.org/lhc12/hl1486.git" (no quotes), and the whole program will be cloned to a folder named "hl1486" under your Git destination path (as shown on your Git Bash window).
2. Open Intellij IDEA, click "File" on the menu, then "Open...", and then select the project named "hl1486" in the directory where it is just downloaded.
3. Run the java class "Examples" under "src" folder of the project "hl1486", and you will see messages in the console while it's running, and after the program ends, you can also check an output file "optionprices.txt" under the downloaded "hl1486" folder.
4. You can also run those test classes under "test" folder in the project, as well as look into the actual code, all being well commented.


Note:

Compared to the original simulation assignment, only the implementation of "NormRandGenerator" class is changed by modifying the "getVector" method and adding a "createBatch" method that uses GPU to generate normal random variables.


If you have any question, please be sure to ask me. Any advice is also greatly appreciated. Thank you!

@author Haicheng Lu, HL1486, hl1486@nyu.edu