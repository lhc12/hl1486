import junit.framework.TestCase;

public class Test_StatsCollector extends TestCase {
	
	// test the StatsCollector class
	public void testStatsCollector() {
		
		StatsCollector stats = new StatsCollector();
		
		stats.add(1);
		stats.add(2);
		stats.add(3);
		
		System.out.println(stats.getAverage());
		System.out.println(stats.getStdDev());
		
	}

}
