import junit.framework.TestCase;

public class Test_AntitheticRandomVectorGenerators extends TestCase {
	
	int n = 5;
	RandomVectorGenerator rvg;
	double[] v;
	
	// test normal random number vector generator
	public void testNormRandVectorGenerator() {
		
		rvg = new NormRandGenerator(n);
		v = rvg.getVector();
		
		for (int i=0; i<n; i++)
			System.out.format("%12f",v[i]);
		System.out.println();
		System.out.println();
		
	}
	
	// test antithetic random number vector generator
	public void testAntitheticVectorGenerator() {
		
		rvg = new NormRandGenerator(n);
		AntitheticVectorGenerator arvg = new AntitheticVectorGenerator(rvg);
		
		for (int j=0; j<4; j++) {
			v = arvg.getVector();
			for (int i=0; i<n; i++)
				System.out.format("%12f",v[i]);
			System.out.println();
		}
		System.out.println();
		
	}

}
