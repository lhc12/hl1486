import java.util.LinkedList;
import java.util.List;
import org.joda.time.DateTime;
import junit.framework.TestCase;

public class Test_DiscountPayOuts extends TestCase {
	
	LinkedList<Pair<DateTime, Double>> prices = new LinkedList<Pair<DateTime,Double>>();
	PayOut payout;
	Discount Dist = new Discount(new samplePath());
	samplePath path = new samplePath();
	
	// create a sample of stock path for subsequent testing
	class samplePath implements StockPath {
		
		public LinkedList<Pair<DateTime,Double>> getPath() {
			prices.add(new Pair<DateTime,Double>(new DateTime("2014-10-16"), 110.0));
			prices.add(new Pair<DateTime,Double>(new DateTime("2014-10-17"), 120.0));
			prices.add(new Pair<DateTime,Double>(new DateTime("2014-10-18"), 130.0));
			prices.add(new Pair<DateTime,Double>(new DateTime("2014-10-19"), 140.0));
			prices.add(new Pair<DateTime,Double>(new DateTime("2014-10-20"), 150.0));
			return prices;
		}
		public DateTime getStartDate() { return new DateTime("2014-10-16"); }
		public DateTime getEndDate() { return new DateTime("2014-10-20"); }
		public double getR() { return 0.01; }
	}
	
	// test European call option payout and discounting function
	public void testEuroCallOption() {
		
		payout = new EuroCallPayout(140);
		double price = payout.getPayout(path);
		System.out.println(price);
		double discPrice = Dist.getPrice(price);
		System.out.println(discPrice);
		payout = new EuroCallPayout(160);
		System.out.println(payout.getPayout(path));

	}

	// test Asian call option payout and discounting function
	public void testAsianCallOption() {
		
		payout = new AsianCallPayout(120);
		System.out.println(payout.getPayout(path));
		System.out.println(Dist.getPrice(payout.getPayout(path)));
		payout = new AsianCallPayout(140);
		System.out.println(payout.getPayout(path));
				
	}

}
