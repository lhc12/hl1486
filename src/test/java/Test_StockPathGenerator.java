import java.util.List;
import org.joda.time.DateTime;
import junit.framework.TestCase;

public class Test_StockPathGenerator extends TestCase {
	
	// test the path generator based on GBM model
	public void testGBMPathGenerator() {
		
		DateTime startDate = new DateTime();
		int n = 10;
		AntitheticVectorGenerator arvg = new AntitheticVectorGenerator(new NormRandGenerator(n));

		GBMPathGenerator pathGenerator = new GBMPathGenerator(152.35,0.0001,0.01,
				startDate,startDate.plusDays(5),n,arvg);
		List<Pair<DateTime, Double>> path = pathGenerator.getPath();
		for (int i=0; i<path.size(); i++) {
			System.out.printf("%s  %f",path.get(i).getKey().toString(),path.get(i).getValue());
			System.out.println();
		}

	}

}
