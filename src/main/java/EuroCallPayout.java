import java.util.LinkedList;
import org.joda.time.DateTime;

/**
 * Class returning one simulated payout of European call options
 * @author Lhc
 *
 */
public class EuroCallPayout implements PayOut {
	
	private double K;

	/**
	 * Constructor - assign global stock path
	 * @param K	strike price
	 */
	public EuroCallPayout(double K){
		this.K = K;
	}

	/**
	 * Calculate and return the payout of European options
	 * @param path		underlying price path for European options
	 * @return			payout given the strike price and path
	 */
	public double getPayout(StockPath path) {
		LinkedList<Pair<DateTime,Double>> prices = path.getPath();     	// generate a new path of stock price movement
		return Math.max(0, prices.getLast().getValue() - K);      		// the payout is the maximum of 0 and the last price minus strike price
	}


}
