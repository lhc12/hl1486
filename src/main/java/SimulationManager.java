import org.apache.commons.math3.distribution.NormalDistribution;
import org.joda.time.DateTime;

/**
 * Class that manage the calculation of option prices using Monte Carlo simulation
 * @author Lhc
 *
 */
public class SimulationManager {
	
	private StockPath path;
	private PayOut payout;
	private double accuracy;
	private double bound;
	
	/**
	 * Constructor
	 * @param S0          current stock price
	 * @param r           interest rate per day
	 * @param sigma       volatility per day
	 * @param startDate   start date
	 * @param endDate     end date
	 * @param N           number of intervals
	 * @param payout      option payout, specific implementation of PayOut interface
	 * @param confidence  confidence level about the error
	 * @param accuracy    accuracy of the error
	 */
	public SimulationManager(double S0, double r, double sigma, DateTime startDate, DateTime endDate, 
			int N, PayOut payout, double confidence, double accuracy) {
		
		// use the antithetic normal random vector generator
		AntitheticVectorGenerator arvg = new AntitheticVectorGenerator(new NormRandGenerator(N));
		// use the path generator based on Geometric Brownian Motion model
		path = new GBMPathGenerator(S0,r,sigma,startDate,endDate,N,arvg);
		this.payout = payout;
		this.accuracy = accuracy;
		// the inverse cumulative probability of normal distribution under confidence and accuracy requirements, for evaluating error
		this.bound = new NormalDistribution().inverseCumulativeProbability(1-(1-confidence)/2);
	}
	
	/**
	 * @return option price calculated by Monte Carlo simulation
	 */
	public double optionPrice() {
		
		StatsCollector payoutStats = new StatsCollector();  	// tracking the standard deviation of simulated payout
		Discount discount = new Discount(path);             	// calculating discounted price of the simulated price path
		boolean stop = false;                              	// stop sign for the simulation
		int count = 0;                                      	// number of simulation

		while (!stop || count<1000) {							// if not reaching the required simulation times and ensure to run at least 1000 times at first
			count++;
			payoutStats.add(payout.getPayout(path));			// generate a new payout and track its mean and variance
			if (count%1000==0)
				stop = count > Math.pow(bound*payoutStats.getStdDev()/accuracy, 2);    // evaluate the criterion for stopping simulation
			if (count%100000==0)
				System.out.printf("Iterations %d  Price %.4f%n", count, discount.getPrice(payoutStats.getAverage()));
		}

		return discount.getPrice(payoutStats.getAverage());	// return the present price of the expected payout at maturity
	}
	
}