import java.util.Arrays;

/**
 * Class for the Antithetic decorator pattern to generate antithetic pairs of random vectors,
 * implementing RandomVectorGenerator interface
 * @author Lhc
 *
 */
public class AntitheticVectorGenerator implements RandomVectorGenerator {

	private RandomVectorGenerator rvg;
	double[] lastVector;                   // the last random vector

	/**
	 * Constructor - assign global random generator
	 * @param rvg	random vector generator
	 */
	public AntitheticVectorGenerator(RandomVectorGenerator rvg){
		this.rvg = rvg;
	}

	/**
	 * Generate antithetic random number vectors following antithetic rules
	 * @return	an antithetic random number vector
	 */
	public double[] getVector() {
		// generate a new random vector
		if ( lastVector == null ){
			lastVector = rvg.getVector();
			return lastVector;
		} // if the last vector exists, reverse each element of it
		else {
			double[] tmp =Arrays.copyOf(lastVector, lastVector.length);
			lastVector = null;       // in order to generate a new pair next time
			for (int i = 0; i < tmp.length; ++i) { tmp[i] = -tmp[i]; }
			return tmp;
		}
	}

}
