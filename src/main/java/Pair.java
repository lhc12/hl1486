/**
 * General customized pair class, with key and value for each element
 * @author Lhc
 *
 * @param <KeyType>
 * @param <ValueType>
 */
public class Pair<KeyType, ValueType> {
	
	private KeyType Key;
	private ValueType Value;
	
	/**
	 * Constructor - set key and value
	 * @param key
	 * @param value
	 */
	public Pair(KeyType key, ValueType value) {
		Key = key; Value = value;
	}
	
	public KeyType getKey() { return Key; }
	public ValueType getValue() { return Value; }
	
	public void setKey(KeyType key) { Key = key; }
	public void setValue(ValueType value) { Value = value; }

}
