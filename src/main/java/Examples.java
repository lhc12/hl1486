import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.joda.time.DateTime;

/**
 * This class shows two specific examples of calculating option prices using SimulationManager class
 * @author Lhc
 *
 */
public class Examples {
	
	public static void main(String[] args) throws IOException {
		
		// prepare for output
		FileWriter fileWriter = new FileWriter(new File("optionprices.txt"));

		String message1 = "Calculating the European call option price of IBM with maturity 252 days, strike price 165, \n"
				+ "current stock price 152.35, volatility 0.01 per day, and interest rate 0.0001 per day \n"
				+ "by Monte Carlo simulation -- ";
		System.out.println(message1);
		fileWriter.write(message1);
		// instantiate a simulation manager for the European call option example
		SimulationManager IBMEuroCall = new SimulationManager(152.35, 0.0001, 0.01, new DateTime(), new DateTime().plusDays(252), 
				252, new EuroCallPayout(165), 0.96, 0.01);
		// calculate the price through running simulations
		double price = IBMEuroCall.optionPrice();
		String message2 = "When the estimation error is less than 0.01 with probability 0.96, \nthe price is %.4f%n";
		System.out.printf(message2,	price);
		fileWriter.write(String.format(message2, price));
		
		System.out.println("########################################################################################");

		message1 = "Calculating the Asian call option price of IBM with maturity 252 days, strike price 164, \n"
				+ "current stock price 152.35, volatility 0.01 per day, and interest rate 0.0001 per day \n"
				+ "by Monte Carlo simulation -- ";		
		System.out.println(message1);
		fileWriter.write(message1);
		// instantiate a simulation manager for the Asian call option example
		SimulationManager IBMAsianCall = new SimulationManager(152.35, 0.0001, 0.01, new DateTime(), new DateTime().plusDays(252), 
				252, new AsianCallPayout(164), 0.96, 0.01);
		// calculate the price through running simulations
		price = IBMAsianCall.optionPrice();
		System.out.printf(message2, price);
		fileWriter.write(String.format(message2, price));
		
		fileWriter.close();
		
	}

}
