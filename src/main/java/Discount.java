import org.joda.time.Days;

/**
 * Class that calculates the discounted value (present value) of payout from the class implementing StockPath interface
 * @author Lhc
 *
 */
public class Discount {
	
	StockPath path;

	/**
	 * Constructor - assign global stock path
	 * @param path		stock path including related parameters
	 */
	public Discount(StockPath path) { this.path = path;	}


	/**
	 * Method to calculate the discounted present value of given payout at maturity
 	 * @param payout	payout at maturity
	 * @return			present value of payout calculated by time and interest rate for the stock path
	 */
	public double getPrice(double payout) {
		// the discounted price is exp(-rT)*payout
		return payout * Math.exp(-path.getR()*Days.daysBetween(path.getStartDate(), path.getEndDate()).getDays());
	}

}
