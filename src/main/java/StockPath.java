import java.util.LinkedList;
import org.joda.time.DateTime;

/**
 * Interface for simulating the path of stock price movement
 * @author Lhc
 *
 */
public interface StockPath {
	
	public LinkedList<Pair<DateTime,Double>> getPath();
	public DateTime getStartDate();
	public DateTime getEndDate();
	public double getR();

}
