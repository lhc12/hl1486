/**
 * Interface for getting the payout from a StockPath class
 * @author Lhc
 *
 */
public interface PayOut {
	
	public double getPayout(StockPath path);

}
