import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import static org.bridj.Pointer.allocateFloats;

/**
 * Class generating a vector of normal random numbers
 * @author Lhc
 *
 */
public class NormRandGenerator implements RandomVectorGenerator {

	private final int n = 1000000;					// number of one list of uniform random numbers to be sent to GPU, there are two such lists, totally 2000000
	private int N;										// the length of random number vector needed to be returned
	private LinkedList<Float> normRandNumbers;		// list of 2000000 normal random numbers
	private Iterator<Float> itr;						// iterator of normRandNumbers

	/**
	 * Constructor - initialize parameters and generate the first batch of random numbers
	 * @param N     the length of the random number vector
	 */
	public NormRandGenerator(int N) {
		this.N = N;
		createBatch();		// for the first time initialize the list normRanNumbers and its iterator itr
	}

	/**
	 * Method to slice normRandNumbers to return  the requested number of normal random numbers
	 * @return	a vector of normal random numbers of length N
	 */
	public double[] getVector() {
		double[] vector = new double[N];
		for ( int i=0; i<N; ++i)
			// fetch one normal random number each time fro normRandNumbers to populate the vector to be returned
			if (itr.hasNext())
				vector[i] = itr.next();
			// if normRandNumbers has been depleted, create a new batch of normal random numbers from GPU
			else {
				createBatch();
				vector[i] = itr.next();
			}
		return vector;
	}

	/**
	 * Method to generate a new batch of normal random numbers using GPU
	 */
	private void createBatch() {
		// Creating the platform which is our computer.
		CLPlatform clPlatform = JavaCL.listPlatforms()[0];
		// Getting the GPU device
		CLDevice device = clPlatform.listGPUDevices(true)[0];
		// Making a context
		CLContext context = JavaCL.createContext(null, device);
		// Making a default FIFO queue.
		CLQueue queue = context.createDefaultQueue();

		// Read the program sources and compile them:
		// This kernel function convert two uniform random numbers to two normal random numbers using Box-Muller transform
		String src = "__kernel void unif_to_norm(__global const float* u1, __global const float* u2, __global float* z1, __global float* z2, int n) \n" +
				"{\n" +
				"    int i = get_global_id(0);\n" +
				"    if (i >= n)\n" +
				"        return;\n" +
				"\n" +
				"    z1[i] = sin(2*M_PI*u2[i])*sqrt(-2*log(u1[i]));\n" +
				"    z2[i] = cos(2*M_PI*u2[i])*sqrt(-2*log(u1[i]));\n" +
				"}";
		// Creating a program
		CLProgram program = context.createProgram(src);
		// Build the program
		program.build();
		// Creating a kernel
		CLKernel kernel = program.createKernel("unif_to_norm");

		// Allocate two native memory Pointers for storing uniform random numbers
		final Pointer<Float>
				iPtr1 = allocateFloats(n),
				iPtr2 = allocateFloats(n);
		// Populate the two lists with uniform random numbers ranging from 0 to 1
		Random rnd = new Random();
		for (int i = 0; i < n; i++) {
			iPtr1.set(i, rnd.nextFloat()+Float.MIN_NORMAL);		// +Float.MIN_NORMAL to avoid 0, because 0 will result in
			iPtr2.set(i, rnd.nextFloat()+Float.MIN_NORMAL);		// the normal random number transformed by Box-Muller being NA or Infinity
		}

		// Create OpenCL input buffers (using the native memory pointers iPtr1 and iPtr2) :
		CLBuffer<Float>
				in1 = context.createFloatBuffer(CLMem.Usage.Input, iPtr1),
				in2 = context.createFloatBuffer(CLMem.Usage.Input, iPtr2);

		// Create an OpenCL output buffer :
		CLBuffer<Float>
				out1 = context.createFloatBuffer(CLMem.Usage.Output, n),
				out2 = context.createFloatBuffer(CLMem.Usage.Output, n);

		// Set input and output arguments for the kernel function
		kernel.setArgs(in1, in2, out1, out2, n);

		// Create event and enqueue the command to execute a kernel on a device
		CLEvent event = kernel.enqueueNDRange(queue, new int[]{n});

		// Read the two normal random numbers lists from the output
		final Pointer<Float>
				oPtr1 = out1.read(queue,event),
				oPtr2 = out2.read(queue,event);

		// Initialize and populate normRandNumbers, combining the two normal random numbers, and reset the iterator
		normRandNumbers = new LinkedList<Float>();
		for ( int i = 0; i < n; ++i) {
			normRandNumbers.add(oPtr1.get(i));
			normRandNumbers.add(oPtr2.get(i));
		}
		itr = normRandNumbers.iterator();

	}

}
