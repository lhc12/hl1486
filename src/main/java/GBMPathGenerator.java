import java.util.LinkedList;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 * Class that executes the Geometric Brownian Motion model to simulate the stock price movement path
 * @author Lhc
 *
 */
public class GBMPathGenerator implements StockPath {

	private double S0;
	private double r; 
	private double sigma; 
	private DateTime startDate;
	private DateTime endDate;
	private int N; 
	private RandomVectorGenerator rvg; 
	
	/**
	 * Constructor - input parameters for GBM model
	 * @param S0          current stock price
	 * @param r           interest rate per day
	 * @param sigma       volatility per day
	 * @param startDate   start date
	 * @param endDate     end date
	 * @param N           number of intervals
	 * @param rvg         random number vector generator
	 */
	public GBMPathGenerator(double S0, double r, double sigma, 
							DateTime startDate, DateTime endDate, 
							int N, RandomVectorGenerator rvg) {
		
		this.S0 = S0; this.r = r; this.sigma = sigma;
		this.startDate = startDate;	this.endDate = endDate;
		this.N = N;	this.rvg = rvg;
	
	}

	/**
	 * Method to return one simulated path which consists of pairs of datetime and price
	 */
	public LinkedList<Pair<DateTime, Double>> getPath() {
		
		double[] rnd = rvg.getVector();
		DateTime current = new DateTime(startDate.getMillis());
		int dT = (int) ((endDate.getMillis() - startDate.getMillis()) / N);           // time difference between start date and end date
		double dD = Days.daysBetween(startDate, endDate).getDays() / (double) N;      // days between start date and end date
		LinkedList<Pair<DateTime, Double>> path = new LinkedList<Pair<DateTime,Double>>();
		path.add(new Pair<DateTime, Double>(current, S0));        // add the current datetime and price
		for (int i=1; i<N; i++) {
			current = current.plusMillis(dT);                     // update datetime for one step
			// calculate one new step based on GBM model, and add the new datetime and price to the path
			path.add(new Pair<DateTime, Double>(current,
				path.get(path.size()-1).getValue()*Math.exp((r-sigma*sigma/2)*dD+sigma*Math.sqrt(dD)*rnd[i-1])));
		}
		return path;
	
	}
	
	public DateTime getStartDate() { return startDate; }
	public DateTime getEndDate() { return endDate; }
	public double getR() { return r; }

}
