/**
 * Interface for generating a vector of random numbers according to any specific distribution
 * @author Lhc
 *
 */
public interface RandomVectorGenerator {
	
	public double[] getVector();

}
