import java.util.Iterator;
import java.util.LinkedList;
import org.joda.time.DateTime;

/**
 * Class returning one simulated payout of Asian call options
 * @author Lhc
 *
 */
public class AsianCallPayout implements PayOut {
	
	private double K;      // strike price

	/**
	 * Constructor - assign global strike price
	 * @param K
	 */
	public AsianCallPayout(double K){
		this.K = K;
	}

	/**
	 * Calculate and return the payout of Asian options
	 * @param path		underlying price path for Asian options
	 * @return			payout given the strike price and path
	 */
	public double getPayout(StockPath path) {
		LinkedList<Pair<DateTime,Double>> prices = path.getPath();     // generate a new path of stock price movement
		double average = 0;
		int i = 0;
		Iterator<Pair<DateTime,Double>> itr = prices.iterator();
		// iterate over the path and compute the average price
		while (itr.hasNext()) {
			average = average * i / (i + 1) + itr.next().getValue() * 1 / (i + 1);
			i++;
		}
		return Math.max(0, average - K);       // the payout is the maximum of 0 and the average price minus strike price
	}


}
