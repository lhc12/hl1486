/**
 * Class for tracking (updating) the mean and standard deviation of an expanding vector of numbers
 * @author Lhc
 *
 */
public class StatsCollector {
	
	private double num = 0;
	private double mean = 0;
	private double meanSquare = 0;

	/**
	 * Add a new value and update mean and squared mean
	 * @param newValue
	 */
	public void add(double newValue) {
		num++;																	// update total number of values
		mean = (num-1)/num*mean + 1/num*newValue;                         	// update mean
		meanSquare = (num-1)/num*meanSquare + 1/num*newValue*newValue;   	// update the squared mean
	}

	/**
	 * Return average for all added values
	 * @return	average for all added values
	 */
	public double getAverage() {
		return mean;
	}

	/**
	 * Return standard deviation for all added values
	 * @return	standard deviation for all added values
	 */
	public double getStdDev() {
		return Math.sqrt(meanSquare-mean*mean);
	}

}
